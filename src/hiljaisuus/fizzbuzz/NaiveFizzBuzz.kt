package hiljaisuus.fizzbuzz

fun main() {
    var returnValue : String = ""
    for (i in 1..100 step 1) {
        if (i % 3 == 0 && i % 5 == 0)
            returnValue = "FizzBuzz"
        else if (i % 3 == 0)
            returnValue = "Fizz"
        else if (i % 5 == 0)
            returnValue = "Buzz"
        else
            returnValue = i.toString()
        println(returnValue)
    }
}