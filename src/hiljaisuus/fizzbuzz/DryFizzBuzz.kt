package hiljaisuus.fizzbuzz

fun main() {

    fun divide (x: Int, y: Int) = x % y == 0

    for (i in 1..100 step 1) {
        val fizz = divide(i, 3)
        val buzz = divide(i, 5)
        if (fizz && buzz)
            println("FizzBuzz")
        else if (fizz)
            println("Fizz")
        else if  (buzz)
            println("Buzz")
        else
            println(i)
    }
}